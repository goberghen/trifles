#!/bin/sh
#################################################################
# Simple shell implementation of vrms utility (for Debian only)	#
# It will show and count your contrib and non-free packages.	#
# Made by goberghen(@gmail.com)									#
#################################################################

# Old variables (aptitude is slow and not universal):
# Listcontrib=$(aptitude search -F %p ~i~scontrib)
# Listnonfree=$(aptitude search -F %p ~i~snon-free)
# Countcontrib=$(aptitude search ~i~scontrib | wc -l)
# Countnonfree=$(aptitude search ~i~snon-free | wc -l)

# define colors
red="[31m" lred="[1;31m" grn="[32m" lgrn="[1;32m"
ylw="[33m" lylw="[1;33m" cyn="[36m" lcyn="[1;36m"
blu="[34m" lblu="[1;34m" prp="[35m" lprp="[1;35m"
rst="[0m"

# define searching variables
Listcontrib=$(dpkg-query -W -f='${Section}\t${Package}\n' |\
	grep ^contrib | awk '{print $2}')
Listnonfree=$(dpkg-query -W -f='${Section}\t${Package}\n' |\
	grep ^non-free | awk '{print $2}')
if [ "$Listcontrib" != "\n" ]
then
	Countcontrib=$(echo "$Listcontrib" | wc -l)
else
	Countcontrib=""
fi

if [ "$Listnonfree" != "\n" ]
then
	Countnonfree=$(echo "$Listnonfree" | wc -l)
else
	Countnonfree=""
fi

if [ -n dpkg ] && [ -n "$(grep -i debian /etc/*release)" ]
then
	if [ "$Countcontrib" -ne "0" ] || [ "$Countnonfree" -ne "0" ]
	then
		echo "$lblu""Contrib:$rst\n$Listcontrib"
		echo "$lblu""Non-free:$rst\n$Listnonfree"
		echo $lprp"----------------------"$rst
		if [ "$Countcontrib" -eq 1 ]
		then
			echo $lblu"Contrib: $lred$Countcontrib$rst package."
		else
			echo $lblu"Contrib: $lred$Countcontrib$rst packages."
		fi
		if [ "$Countnonfree" -eq 1 ]
		then
			echo $lblu"Non-free: $lred$Countnonfree$rst package."
		else
			echo $lblu"Non-free: $lred$Countnonfree$rst packages."
		fi
		echo $lred"Stop right here you proprietary scum!"$rst
	elif [ "$Countcontrib" -eq "0" ] && [ "$Countnonfree" -eq "0" ]
	then
		echo $lblu"Contrib: $lgrn$Countcontrib$rst package(s)."
		echo $lblu"Non-free: $lgrn$Countnonfree$rst package(s)."
		echo $lprp"You're free as in freedom, comrade! RMS would be proud!"$rst
	else
		echo $red"You really should install Debian GNU/Linux, comrade."$rst
	fi
else
	echo "Sorry, this version of vrms.sh is made only for Debian."
	echo "You need to have the dpkg package manager installed to use vrms.sh."
fi

