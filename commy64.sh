#!/bin/sh
# POSIX now!
# Demonstration: https://asciinema.org/a/0s4a1paqew863ivsqntrehx0i
[ "$TERM" = "xterm" ] && TERM="xterm-256color"
[ "$(uname)" = "Linux" ] && System='GNU+Linux'
echo_c()
{
	w=$(stty size | cut -d" " -f2)       # width of the terminal
	l=${#1}                              # length of the string
	printf "%"$((l+(w-l)/2))"s\n" "$1"   # print string padded to proper width (%Ws)
}
printf '\033]2;COMMODORE 64\007'
clear
tput setab 61
tput setaf 147
tput el
echo_c "**** $System $($SHELL --version | sed -n 1p | awk '{print $1,$2,$4}' |\
tr [:lower:] [:upper:] | sed 's/\ $//') ****"
tput el
echo_c "$(free -k | awk 'NR==2 {print $2}')K RAM SYSTEM $(free -b |\
awk 'NR==2 {print $4}') BYTES FREE"
tput ed
printf "READY.\n"

doLoad()
{
	read loadgame
	# loadgame=$(echo ${loadgame} | awk '{print $2}' | tr [:upper:] [:lower:])
	loadgame=$(echo ${loadgame} | awk '{print $2}')
	if command -v ${loadgame} > /dev/null 2>&1
	then
		printf "OK\n"
	elif gtk-launch ${loadgame} > /dev/null 2>&1
	then
		printf "OK\n"
	else
		tput setab 9
		tput setaf 0
		tput el
		echo_c "ERROR! Can't load $(tput smul)${loadgame}$(tput rmul)!"
		tput sgr0
		return 1
	fi
}

doRun()
{

	read rungame
	if [ "$rungame" = "run" ] && [ -n "$loadgame" ]
	then
		clear
		i=0
		repeat=400
		while [ "$i" -lt "$repeat" ]
		do
			tput setab $(shuf -i 0-255 -n 1)
			tput setaf $(shuf -i 0-255 -n 1)
			echo ""
			tput ed
			tput sgr0
			true $(( i=i+1 ))
		done
		clear
		. ~/.bashrc
		${loadgame}
	else
		tput setab 9
		tput setaf 0
		tput el
		echo_c "ERROR! Type 'run' to run a program."
		tput sgr0
		return 1
	fi
}
doLoad && doRun

