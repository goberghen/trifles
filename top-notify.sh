#!/bin/bash
# Used memory and percent of memory used
usmem=$(free -h | awk '/Mem:/ {print $3}')
pusmem=$(free | awk '/Mem:/ {print int($3/$2 * 100.0)}')
# Used swap and percent of swap used
usswap=$(free -h | awk '/Swap:/ {print $3}')
puswap=$(free | awk '/Swap:/ {print int($3/$2 * 100.0)}')
cpusage=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}')
# Notification
notify-send -h int:value:$cpusage "<b>CPU used:</b>" %p
notify-send -h int:value:$pusmem "<b>Memory used:</b>" "$usmem %p"
notify-send -h int:value:$puswap "<b>Swap used:</b>" "$usswap %p"
