#!/bin/sh
# vim: set ts=4 sw=4 tw=0 ft=sh fdm=marker noet :
#################################################################################
## Name: shootan.sh
## Author: goberghen, 2015
## Description: Simple wrapper for scrot and image uploading scripts.
## Depends: scrot, curl, xclip, libnotify-bin (optional for notifications),
# imagemagick (for "import" instead of "scrot"), azpainter (optional for
# paint/edit functionality)
#################################################################################

######## BEGIN CONFIGURABLE VARIABLES {{{ #########
Notify=true
Service='0x0'
Delay=0
AScrotcmd="scrot"
WScrotcmd="scrot -bu"
# SScrotcmd="scrot -s"
SScrotcmd="import"
Scrotpath=""
Paint="false"
ImgurKey=c9a6efb3d7932fd
######## END CONFIGURABLE VARIABLES }}} #########
if [ -z "$*" ]
then
	Help=true
fi
