#!/bin/sh
# Tmpbg=/tmp/screenshot.png
Tmpbg="/home/gbrghn/pic/scrot/locked3.png"
# Lockicon=$HOME/.icons/elementary-xfce-gbrghn/locker2.png
Layout=$(setxkbmap -query | grep layout | awk '{print $2}')

revert()
{
	xset dpms 0 0 0 ; xset -dpms
}

# scrot $Tmpbg
# convert $Tmpbg -blur 0x6 $Tmpbg
# convert $Tmpbg $Lockicon -gravity center -composite -matte $Tmpbg
# trap revert SIGHUP SIGINT SIGTERM
trap revert HUP INT TERM
xset dpms 5 5 5 ; xset +dpms
setxkbmap -layout us
i3lock -n -f -i $Tmpbg
revert
setxkbmap -layout "$Layout"
# [ -f "$Tmpbg" ] && [ "$Tmpbg" = "/tmp/screenshot.png" ] && rm "$Tmpbg"
