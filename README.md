Trifles
=======

Repo for my little shitty scripts. Mostly bash or POSIX shell.

What is this repository for?
----------------------------
[I don't know.](https://youtu.be/RrWCpPyI5pA)

Just clone it and use. I recommend to use [xstow](http://xstow.sourceforge.net/) for managing scripts and dotfiles.

ToDo
----
* List and descriptions

### [Fossil](https://www.fossil-scm.org) repo file is not available now.
___
Licensed under [LGPLv3](https://bitbucket.org/goberghen/trifles/src/HEAD/LICENSE.md?at=master) where possible.
