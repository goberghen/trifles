#!/bin/sh
# This script is used to copy all needed scripts from "dotfiles" repo/directory here
cp ../dotfiles/bin/bin/desktopconsole.sh ./
cp ../dotfiles/bin/bin/envinfo.sh ./
cp ../dotfiles/bin/bin/epoch.sh ./
cp ../dotfiles/bin/bin/fsuae.sh ./
cp ../dotfiles/bin/bin/gamemode.sh ./
cp ../dotfiles/bin/bin/konsolka.sh ./
cp ../dotfiles/bin/bin/kopimashin.sh ./
cp ../dotfiles/bin/bin/locker.sh ./
cp ../dotfiles/bin/bin/pacmansuy.sh ./
cp ../dotfiles/bin/bin/plashka.sh ./
cp ../dotfiles/bin/bin/rname.sh ./
cp ../dotfiles/bin/bin/shootan.sh ./
cp ../dotfiles/bin/bin/teaparty.sh ./
cp ../dotfiles/bin/bin/top-notify.sh ./
cp ../dotfiles/bin/bin/vrms.sh ./
cp ../dotfiles/bin/bin/xkcd-show.sh ./
cp ../dotfiles/bin/bin/fun/anime.sh ./
cp ../dotfiles/bin/bin/fun/commy64.sh ./
cp ../dotfiles/bin/bin/fun/xmastree.sh ./
