#!/bin/bash
free_mem=$(free -m | awk 'NR==2 {print $6}')

if [ "$free_mem" -gt "1000" ]
then
	echo "Да у тебя ещё в запасе целых $free_mem МБ памяти."
	tput bold ; tput setaf 2 ; echo "Плашку не покупать!" ; tput sgr0
else
	echo "Осталось всего $free_mem МБ памяти."
	tput bold ; tput setaf 1 ; echo "Плашку покупать!" ; tput sgr0
fi

