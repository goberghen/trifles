#!/bin/sh
# POSIX now!
Fsuaedir="$HOME/doc/FS-UAE/"

printf '\033]2;FS-UAE Amiga Emulator\007'
[ "$TERM" = "xterm" ] && TERM="xterm-256color"
commy_rainbow()
{
	i=0
	repeat=400
	while [ "$i" -lt "$repeat" ]
	do
		tput setab $(shuf -i 0-255 -n 1)
		tput setaf $(shuf -i 0-255 -n 1)
		echo ""
		tput ed
		tput sgr0
		true $(( i=i+1 ))
	done
	clear
}

cd $Fsuaedir

echo_c()
{
	w=$(stty size | cut -d" " -f2)       # width of the terminal
	l=${#1}                              # length of the string
	printf "%"$((l+(w-l)/2))"s\n" "$1"   # print string padded to proper width (%Ws)
}

show_menu()
{
	clear
	tput setaf 61
	tput setab 147
	tput el
	printf "FS-UAE Amiga Emulator"
	tput setab 61
	tput setaf 147
	printf "\n"
	tput el
	echo_c "**** $($SHELL --version | sed -n 1p | awk '{print $1,$2}' |\
	tr [:lower:] [:upper:] | sed s/,$//) V$($SHELL --version |\
	sed -n 1p | awk '{print $4}' | tr [:lower:] [:upper:]) ****"
	tput el
	echo_c "$(free -k | awk 'NR==2 {print $2}')K RAM SYSTEM $(free -k |\
	awk 'NR==2 {print $6}') KILOBYTES FREE"
	tput ed
	printf "READY.\n"
	printf "CHOOSE AMIGA MODEL TO LOAD:\n"
	tput setaf 147
	tput setab 61
	tput el
	printf "[l/L] Last\n"
	tput setaf 61
	tput setab 147
	tput el
	printf "[1] AMIGA  500\n"
	tput setaf 147
	tput setab 61
	tput el
	printf "[2] AMIGA  600\n"
	tput setaf 61
	tput setab 147
	tput el
	printf "[3] AMIGA 1000\n"
	tput setaf 147
	tput setab 61
	tput el
	printf "[4] AMIGA 1200\n"
	tput setaf 61
	tput setab 147
	tput el
	printf "[5] AMIGA 4000\n"
	tput setaf 147
	tput setab 61
	tput el
	printf "[q/Q] EXIT\n"
	tput setaf 61
	tput setab 147
	tput el
	printf "\n"
	tput setaf 147
	tput setab 61
	read amigamodel
}
show_menu
case $amigamodel in
	l|L)
		commy_rainbow && fs-uae &
	;;
	1)
		commy_rainbow && cd ./Configurations/ &&
		ln -frs ./a-500.ini ./Default.fs-uae &&
		cd $Fsuaedir && fs-uae
	;;
	2)
		commy_rainbow && cd ./Configurations/ &&
		ln -frs ./a-600.ini ./Default.fs-uae &&
		cd $Fsuaedir && fs-uae
	;;
	3)
		commy_rainbow && cd ./Configurations/ &&
		ln -frs ./a-1000.ini ./Default.fs-uae &&
		cd $Fsuaedir && fs-uae
	;;
	4)
		commy_rainbow && cd ./Configurations/ &&
		ln -frs ./a-1200.ini ./Default.fs-uae &&
		cd $Fsuaedir && fs-uae
	;;
	5)
		commy_rainbow && cd ./Configurations/ &&
		ln -frs ./a-4000.ini ./Default.fs-uae &&
		cd $Fsuaedir && fs-uae
	;;
	q|Q)
		tput sgr0
		tput ed
		clear
		exit 0
	;;
	*)
		tput setaf 0
		tput setab 9
		tput el
		echo "WRONG VALUE!"
		sleep 1
		show_menu
	;;
esac
