#!/bin/sh
# vim: set ts=4 sw=4 tw=0 ft=sh fdm=marker noet :
#################################################################################
## Name: shootan.sh
## Author: goberghen, 2015
## Description: Simple wrapper for scrot and image uploading scripts.
## Depends: scrot, curl, xclip, libnotify-bin (optional for notifications),
# imagemagick (for "import" instead of "scrot"), azpainter (optional for
# paint/edit functionality)
#################################################################################
# Define colors
red="[31m" ; lred="[1;31m" ; grn="[32m" ; lgrn="[1;32m" ; ylw="[33m"
lylw="[1;33m" ; cyn="[36m" ; lcyn="[1;36m" ; blu="[34m" ; lblu="[1;34m"
prp="[35m" ; lprp="[1;35m" ; rst="[0m"

######## BEGIN DO-NOT-CHANGE {{{ ########
Scriptname=$(basename "$0")
Scrotcmd=""
All=false
Select=false
Window=false
Upload=false
Clipboard=false
Help=false
# No uploading from file by default
File=""
######## END DO-NOT CHANGE }}} ########

######## BEGIN CONFIGURABLE VARIABLES {{{ #########
Notify=true
Service='0x0'
Delay=0
AScrotcmd="scrot"
WScrotcmd="scrot -bu"
# SScrotcmd="scrot -s"
SScrotcmd="import"
Scrotpath=""
Paint="false"
ImgurKey=c9a6efb3d7932fd
######## END CONFIGURABLE VARIABLES }}} #########

if [ -z "$*" ]
then
	Help=true
fi

# Scrotpath based on user settings and current Pictures directory {{{
if [ -n "$(command -v xdg-user-dir)" ] && [ -z "$Scrotpath" ]
then
	if [ -z "$(xdg-user-dir PICTURES)" ]
	then
		mkdir -p "$(xdg-user-dir)"/Pictures/scrot/
		Scrotpath="$(xdg-user-dir)/Pictures/scrot/scrot-$(date +%s).png"
	elif [ -n "$(xdg-user-dir PICTURES)" ]
	then
		mkdir -p "$(xdg-user-dir PICTURES)"/scrot/
		Scrotpath="$(xdg-user-dir PICTURES)/scrot/scrot-$(date +%s).png"
	fi
elif [ -d "$HOME/Pictures/" ]
then
	mkdir -p "$HOME/Pictures/scrot/"
	Scrotpath="$HOME/Pictures/scrot/scrot-$(date +%s).png"
else
	mkdir -p "$HOME/pic/scrot/"
	Scrotpath="$HOME/pic/scrot/scrot-$(date +%s).png"
fi
# }}}
# Setting available options {{{
# Getopt works wrong without this ↓ line
# set -o errexit -o noclobber -o nounset -o pipefail
set -o errexit -o noclobber -o nounset
Opts=$(getopt -o habcpswuD:S:F: --long help,all,buffer,clipboard,paint,select,window,upload,delay:,service:,file: -- "$@")
eval set -- "$Opts"
while true ; do
	case "$1" in
		-h|--help) Help=true ; shift 1 ;;
		-a|--all) All=true ; Scrotcmd=$AScrotcmd ; shift 1 ;;
		-s|--select) Select=true ; Scrotcmd=$SScrotcmd ; shift 1 ;;
		-w|--window) Window=true ; Scrotcmd=$WScrotcmd ; shift 1 ;;
		-c|--clipboard) Clipboard=true ; shift 1 ;;
		-p|--paint) Paint=true ; shift 1 ;;
		-u|--upload) Upload=true ; shift 1 ;;
		-D|--delay) Delay="$2" ; shift 2 ;;
		-S|--service) Upload=true ; Service="$2" ; shift 2 ;;
		-F|--file) File="$2" ; Scrotcmd="" ; shift 2 ;;
		--) shift ; break ;;
		*) echo "Nope!" ; showUsage ; exit 1
	esac
done
# }}}
# showUsage {{{
showUsage()
{
	echo "${lylw}Usage:${rst} ${lgrn}${Scriptname}${rst} ${lprp}<OPTIONS>${rst}"
	echo "${lprp}Options:${rst}"
	echo "  -h | --help                   display this help"
	echo "  -a | --all                    screenshot of the entire screen"
	echo "  -s | --select                 select area for a screenshot"
	echo "  -w | --window                 active window screenshot"
	echo "  -c | --clipboard              save picture right in the clipboard"
	echo "  -p | --paint                  edit picture"
	echo "  -u | --upload                 toggle uploading to the file hosting"
	echo "  -F | --file ${lblu}<file>${rst}            upload a file"
	echo "  -D | --delay ${lblu}<num>${rst}            delay before screenshot (default 0)"
	echo "  -S | --service ${lblu}<service>${rst}      choose the upload service \
(0x0, pstb, imgur, cockfile, choose)"
}
# }}}
if $Help
then
	showUsage ; exit 0
fi

if [ -z "$File" ] && [ "$Upload" = "true" ]
then
	Scrotpath="/tmp/scrot-$(date +%s).png"
elif [ "$Upload" = "false" ]
then
	Scrotpath="$Scrotpath" # DISGUSTING!
else
	Scrotpath="$File"
fi
# If we're uploading a file, upload should be turned on automatically.
if [ -n "$File" ]
then
	Upload=true
fi

#BScrotpath=$(basename "$Scrotpath" | sed -e 's/[^a-zA-Z0-9._-]/-/g')

# upload0x0 {{{
upload0x0()
{
	Servicename="0x0.st"
	echo "${lylw}Uploading${rst} $Scrotpath ${lylw}to $Servicename ...$rst"
	Output=$(curl -s -F file=@"$Scrotpath" https://0x0.st)
}
# }}}
# uploadPstb{{{
uploadPstb()
{
	Servicename="pstb"
	echo "${lylw}Uploading${rst} $Scrotpath ${lylw}to $Servicename ...$rst"
	Output=$(< "$Scrotpath" curl -sLT - https://pstb.win | awk '{print $1}' )
}
# }}}
# uploadImgur {{{
uploadImgur()
{
	Servicename="imgur"
	client_id="${IMGUR_CLIENT_ID:=$ImgurKey}"
	echo "${lylw}Uploading${rst} $Scrotpath ${lylw}to $Servicename ...$rst"
	Output=$(curl -s -H "Authorization: Client-ID $client_id" -H "Expect: " -F "image=@$Scrotpath" https://api.imgur.com/3/image.xml)
	Output="${Output##*<link>}"
	Output="${Output%%</link>*}"
	# Deletion link and error handling is not implemented (yet?).
}
# }}}
# uploadCockfile{{{
uploadCockfile()
{
	Servicename="cockfile"
	echo "${lylw}Uploading${rst} $Scrotpath ${lylw}to $Servicename ...$rst"
	Output="$(curl -s -F files[]=@$Scrotpath https://cockfile.com/upload.php | jq -r '.files.[].url')"
}
# }}}

uploadChoose()
{
	Servicename="$(kdialog --geometry=370x370 --title 'Shootan.sh' --radiolist \
	'Выбери хостинг:' 0x0 0x0 on pstb pstb off imgur imgur off cockfile cockfile off 2>/dev/null)"
	Service="$Servicename"
}

# paintPic {{{
paintPic()
{
	# lazpaint "$Scrotpath"
#	azpainter "$Scrotpath"
	/usr/bin/gwenview "$Scrotpath"
}
#}}}
######## MAIN BEGIN ########
# 0.2 sec delay is a shitty workaround for scrot -s,
if [ "$SScrotcmd" = "scrot -s" ]
then
	sleep 0.2
fi

sleep "$Delay"
if $Clipboard
then
	import png:- | xclip -selection primary -t image/png &&
	xclip -o | xclip -selection clipboard -t image/png
	echo "${lblu}Scrotted to${rst} the clipboard" &&
	if $Notify
	then
		notify-send "Scrotted to" "clipboard"
	fi
elif [ "$Upload" = "false" ] && [ "$Clipboard" = "false" ]
then
	$Scrotcmd "$Scrotpath" &&
	echo "${lblu}Scrotted to${rst} $Scrotpath" &&
	echo "$Scrotpath" | xclip -selection primary &&
	xclip -o | xclip -selection clipboard &&
	if $Notify
	then
		notify-send "Scrotted to" "$Scrotpath"
	fi
	if [ "$Paint" = "true" ]
	then
		paintPic
	fi
elif $Upload && $All || $Window || $Select || [ -n "$File" ]
then
	if [ -n "$Scrotcmd" ]
	then
		$Scrotcmd "$Scrotpath"
	fi
	if [ "$Paint" = "true" ]
	then
		paintPic
	fi
	if [ "$Service" = "choose" ]
	then
		uploadChoose
		if [ "$Service" = "0x0" ]
		then
			upload0x0
		elif [ "$Service" = "pstb" ]
		then
			uploadPstb
		elif [ "$Service" = "imgur" ]
		then
			uploadImgur
		elif [ "$Service" = "cockfile" ]
		then
			uploadCockfile
		else
			echo "${lred}ERROR! Wrong service name.${rst}" ; showUsage ; exit 1
		fi
	fi
	echo "$Output" | xclip -selection primary &&
	xclip -o | xclip -selection clipboard &&
	if $Notify
	then
		notify-send "Uploaded to $Servicename" "You can paste the link now"
	fi
	echo "${lblu}Link:$rst $Output"
else
	echo "${lred}Error! Make sure your options are correct.${rst}"
	showUsage ; exit 1
fi

