#!/bin/bash
## Version for the native xfwm4 compositor
Compstate=`xfconf-query -c xfwm4 -p /general/use_compositing`
case $Compstate in
	true)
		xfconf-query -c xfwm4 -p /general/use_compositing -s false
		notify-send -t 5000 -i applications-games "Gaming mode on" "<b>Compositing is off!</b>"
		;;
	false)
		xfconf-query -c xfwm4 -p /general/use_compositing -s true
		notify-send -t 5000 -i applications-games "Gaming mode off" "<b>Compositing is on!</b>"
		;;
	*)
		echo "Nope"
		notify-send -t 5000 -i applications-games "Gaming mode ???" "<b>Nope, something gone wrong...</b>"
esac
# # Version for compton
# Compstate=`pidof compton`
# if [[ -n $Compstate ]]
# then
#     killall compton &&
#     notify-send -t 5000 -i applications-games "Gaming mode on" "<b>Compositing is off!</b>"
# elif [[ -z $Compstate ]]
# then
#     compton -b &&
#     notify-send -t 5000 -i applications-games "Gaming mode off" "<b>Compositing is on!</b>"
# else
#     echo "Nope"
#     notify-send -t 5000 -i applications-games "Gaming mode ???" "<b>Nope, something gone wrong...</b>"
# fi

