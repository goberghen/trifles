#!/bin/sh
# vim: set ts=4 sw=4 tw=0 fdm=marker noet :
################################################################################
## Name: pacmansuy.sh
## Author: goberghen, 2016-2017
## Description: update wrapper for debian-based distributions
################################################################################
# Makeuseof {{{
# Number of updates, might be usefull for notifications:
#numupdates=$(aptitude search -F %p '~U!~ahold' | wc -l)
# Searching at jobs for "pacmansuy"
# for i in $(atq | awk '{print $1}') ; do echo $i ; at -c $i | grep pacmansuy ; done
# }}}
[ "$TERM" = "xterm" ] && TERM="xterm-256color"
[ "$TERM" = "screen" ] && TERM="screen-256color"
# Define colours
red="[31m" lred="[1;31m" grn="[32m" lgrn="[1;32m" ylw="[33m" lylw="[1;33m"
cyn="[36m" lcyn="[1;36m" blu="[34m" lblu="[1;34m" prp="[35m" lprp="[1;35m"
rst="[0m"
printf '\033]2;PACMANSUY.sh\007'
# Searching pacmansuy jobs in 'at' queue
Nextupgrade="$(for i in $(atq | awk '{print $1}') ; do at -c $i |\
grep --quiet -i pacmansuy ; done && atq | grep "$i" | cut -c 5-23)"
if [ -n "$Nextupgrade" ]
then
	date +"%d %B %Y, %H:%M" --date="$Nextupgrade" > ~/.aptitude/nextupgrade.log
else
	echo "None" > ~/.aptitude/nextupgrade.log
fi
# showLogo {{{
showLogo()
{
	case $TERM in
		linux)
			tput smso ; tput setab 2 ; tput setaf 0
			echo '8""""8 8""""8 8""""8 8""8""8 8""""8 8"""8 8""""8 8   8 8    8                '
			echo '8    8 8    8 8    " 8  8  8 8    8 8   8 8      8   8 8    8    eeeee e   e '
			echo '8eeee8 8eeee8 8e     8e 8  8 8eeee8 8e  8 8eeeee 8e  8 8eeee8    8   " 8   8 '
			echo '88     88   8 88     88 8  8 88   8 88  8     88 88  8   88      8eeee 8eee8 '
			echo '88     88   8 88   e 88 8  8 88   8 88  8 e   88 88  8   88         88 88  8 '
			echo '88     88   8 88eee8 88 8  8 88   8 88  8 8eee88 88ee8   88   88 8ee88 88  8 '
			echo '                                                       Upgrade time!'
			tput sgr0 ;;
		*)
			tput setab 114 ; tput setaf 0
			echo "   ╔═════════════════════════════════════════════════════════════════════\
═════════════════════════════════════╗   "
			echo "   ║           ██████╗  █████╗  ██████╗███╗   ███╗ █████╗ ███╗   ██╗█████\
██╗██╗   ██╗██╗   ██╗                ║   "
			echo "   ║           ██╔══██╗██╔══██╗██╔════╝████╗ ████║██╔══██╗████╗  ██║██╔══\
══╝██║   ██║╚██╗ ██╔╝                ║   "
			echo "   ║           ██████╔╝███████║██║     ██╔████╔██║███████║██╔██╗ ██║█████\
██╗██║   ██║ ╚████╔╝                 ║   "
			echo "   ║           ██╔═══╝ ██╔══██║██║     ██║╚██╔╝██║██╔══██║██║╚██╗██║╚════\
██║██║   ██║  ╚██╔╝  ┌─┐┬ ┬          ║   "
			echo "   ║           ██║     ██║  ██║╚██████╗██║ ╚═╝ ██║██║  ██║██║ ╚████║█████\
██║╚██████╔╝   ██║   └─┐├─┤          ║   "
			echo "   ║           ╚═╝     ╚═╝  ╚═╝ ╚═════╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚════\
══╝ ╚═════╝    ╚═╝  o└─┘┴ ┴          ║   "
			echo "   ╚════════════════════════════════════════════════════════════════════ \
Upgrade time! ═══════════════════════╝   "
			tput sgr0 ;;
	esac
}
# }}}
# aptiClean {{{
aptiClean()
{
HaveAnswer=false
while [ "$HaveAnswer" = false ] ; do
	echo $lblu"Do you want to clean cache and remove orphaned/broken packages?$rst [Y/n] " &&
	read do_clean
	case $do_clean in
		[Yy]|'')
			HaveAnswer=true
			echo $lblu"Cleaning cached deb-files."$rst &&
			sudo -p "${lgrn}Enter your password:$rst " aptitude -w 100 autoclean
			echo $lblu"Removing orphaned and broken packages."$rst &&
			sudo -p "${lgrn}Enter your password:$rst " aptitude -w 100 purge \
$(aptitude search '~g' -F %p)
			echo $lblu"Removing unused configuration files."$rst &&
			sudo -p "${lgrn}Enter your password:$rst " aptitude -w 100 purge '~c'
			echo $lgrn"Done cleaning."$rst;;
		[Nn])
			HaveAnswer=true
			echo $ylw"Cleaning skipped"$rst;;
		*)
			HaveAnswer=false
			echo $lred"Incorrect anwser. Try again!"$rst;;
	esac
done
}
# }}}
# fullUpgrade {{{
fullUpgrade()
{
# do_update='y'
echo ${lblu}"Do you want to upgrade system now?${rst} [Y/n/q] " && read do_update
case ${do_update} in
	[Yy]|'')
		# echo $lblu"Updating apt-file cache."$rst &&
		# [ -e "/usr/bin/apt-file" ] && sudo -p "${lgrn}Enter your password:$rst " apt-file update &&
		# echo $lblu"The apt-file cache is up to date now."$rst ||
		# echo $lred"Something gone wrong, try to run$rst$lprp apt-file update$rst$lred manually."$rst &&
		# echo ${lblu}"Updating package index files."${rst} &&
		echo ${lblu}"Starting full system upgrade."${rst} &&
		sudo -p "${lgrn}Enter your password:$rst " aptitude -w 100 full-upgrade
		sudo -p "${lgrn}Enter your password:$rst " updatedb
		echo 'DISPLAY=:0.0 ; export DISPLAY ; xfce4-terminal -I system-software-update --geometry=114x32 -x ~/bin/pacmansuy.sh' |\
		at now + 4 day
		echo $lgrn"System has been upgraded sucessfully."$rst &&
		date +"%d %B %Y, %H:%M" > ~/.aptitude/lastupgrade.log ; break ||
		echo $lred"Something gone wrong, try to run$rst$lprp aptitude full-upgrade$rst$lred manually."$rst ; break;;
	[Nn])
		echo $lblu"I will ask you tomorrow."$rst &&
		echo 'DISPLAY=:0.0 ; export DISPLAY ; xfce4-terminal -I system-software-update --geometry=114x32 -x ~/bin/pacmansuy.sh' |\
		at now + 1 day
		echo $lred"Press $ylw"Enter"$rst$lred to quit."$rst ;
		read nothing ; return 0;;
	[Qq])
		return 1;;
	*)
		echo $lred"Incorrect anwser. Try again!"$rst;;
esac
}
# }}}
# Main {{{
if [ -n "$(ping -q -w 1 -c 1 ya.ru > /dev/null && echo up)" ]
then
	sudo -p "${lgrn}Enter your password to upgrade the system: ${rst}" true &&
	echo ${lblu}"Updating package index files..."${rst}
	sudo aptitude -w 100 update &&
	echo ${lblu}"Package index is up to date now.\n"${rst} ||
	echo ${lred}"Something gone wrong, try to run${rst}${lprp} aptitude update${rst}${lred} manually."${rst} &&
	Upgradable="$(aptitude search '~U!~v!?origin(Unofficial Multimedia Packages)!~ahold' | wc -l)" &&
	clear
	showLogo
	echo ; echo $lprp"Welcome to pacmansuy.sh!"$rst &&
	echo ${ylw}"Upgradable packages:${rst} ${Upgradable}"
	echo $ylw"Date & Time:$rst $(date +"%d %B %Y, %H:%M")"
	echo $ylw"Last upgrade:$rst $(cat ~/.aptitude/lastupgrade.log)"
	echo $ylw"Next upgrade:$rst $([ -e "$HOME/.aptitude/nextupgrade.log" ] && cat $HOME/.aptitude/nextupgrade.log)"
	fullUpgrade ; aptiClean
	echo $ylw"Done, check $rst${lred}error$rst$ylw messages above (if any)."$rst
	echo $lgrn"Press ${ylw}Enter$rst$lgrn to quit."$rst ;
	read nothing ; return 0
else
	echo $lred"Network is unreachable, try again later."$rst
	echo $lred"Press ${ylw}Enter$rst$lred to quit."$rst ;
	read nothing ; return 1
fi
# }}}
