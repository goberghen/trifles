#!/bin/bash
awk -v lineno="$RANDOM" 'lineno==NR{print;exit}' /usr/share/dict/american-english | sed -e "s/\(.*\)'s/\1/g" | tr '[:upper:]' '[:lower:]'
