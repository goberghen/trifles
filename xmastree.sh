#!/bin/sh
# vim: set ts=4 sw=4 tw=0 noet :
####################################################################################
## Name: xmastree.sh
## Author: goberghen, 2016
## Description: Animated colourful xmas tree :3
## Depends: Nothing.
# ❄❅●○
####################################################################################
Scriptname=$(basename "$0")
clear
Needle="[32m#[0m"
Star="[1;33m*[0m"

drawTree()
{
	printf "\033]2;С новым $(date '+%Y') годом!\007"
tput civis
# Decor1="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m©[0m"
# Decor2="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m®[0m"
# Decor3="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m&[0m"
# Decor4="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m@[0m"
Decor1="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m❄[0m"
Decor2="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m❅[0m"
Decor3="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m●[0m"
Decor4="[$(shuf -i 0-1 -n 1);$(shuf -i 31-36 -n 1)m○[0m"
printf "\033[0;0H"
echo "
                     $Star
                    ${Decor3}${Needle}${Needle}
                   ${Decor3}${Needle}${Needle}${Decor3}${Decor1}
                  ${Needle}*${Decor4}${Needle}${Needle}${Needle}${Needle}
                 ${Needle}${Needle}${Decor4}${Needle}${Needle}${Needle}${Decor4}${Decor1}${Needle}
                ${Needle}${Decor3}${Needle}${Needle}*${Needle}${Needle}${Decor1}${Needle}${Decor2}${Needle}
               ${Decor1}*${Decor4}${Needle}${Needle}${Needle}${Decor3}${Needle}${Decor1}${Needle}${Decor4}${Decor3}${Decor3}
              ${Needle}${Needle}${Decor2}${Decor4}${Decor3}${Needle}${Decor2}*${Needle}${Needle}${Decor2}${Decor4}${Needle}${Needle}${Decor3}
             **${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}*${Needle}${Decor4}${Needle}${Decor4}${Needle}*
            *${Decor2}${Needle}${Needle}${Needle}${Decor4}${Needle}${Decor1}${Decor4}${Decor4}${Decor1}${Decor1}${Decor2}${Needle}${Needle}*${Needle}${Needle}${Needle}
           ${Decor2}${Needle}${Needle}${Needle}${Decor4}${Needle}${Needle}${Decor4}*${Needle}${Decor4}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}
          ${Needle}${Decor3}${Decor4}${Decor3}${Needle}${Needle}${Needle}*${Decor4}${Needle}${Decor1}${Decor3}${Needle}${Needle}${Needle}${Needle}${Needle}${Decor2}${Decor3}${Needle}${Decor2}${Decor3}*
         ${Decor2}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Decor1}${Needle}${Needle}${Needle}${Needle}${Needle}${Decor1}${Decor3}${Needle}${Needle}${Decor3}${Needle}*${Decor3}${Decor3}${Decor4}${Needle}${Decor1}
        *${Needle}${Decor2}${Decor1}${Needle}${Decor3}${Decor1}${Needle}${Decor4}${Needle}${Needle}${Decor3}${Needle}${Decor2}${Needle}${Decor3}${Decor3}${Needle}${Needle}${Decor3}${Needle}${Decor3}${Needle}${Needle}*${Needle}${Needle}
       ${Needle}*${Decor3}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Needle}${Decor3}${Needle}${Decor3}${Decor2}${Needle}${Needle}${Needle}${Needle}*${Needle}${Needle}*${Needle}${Needle}${Needle}${Needle}
      ${Decor1}${Needle}${Needle}${Decor4}${Needle}${Decor2}${Decor2}${Decor2}${Needle}${Needle}${Needle}${Needle}${Needle}${Decor1}${Decor3}${Needle}${Needle}${Decor1}${Needle}${Decor3}${Needle}${Needle}${Needle}${Decor1}${Needle}${Needle}${Needle}${Decor1}${Needle}${Needle}${Decor1}
     ${Decor2}${Needle}${Needle}${Needle}${Needle}${Needle}${Decor4}${Needle}${Needle}${Decor2}${Needle}${Decor1}${Needle}${Needle}${Needle}${Needle}${Decor3}${Needle}${Needle}${Decor4}*${Needle}${Needle}${Decor1}${Decor2}${Needle}${Decor4}${Needle}${Decor3}${Needle}${Decor2}${Decor3}${Needle}
    ${Needle}${Decor2}${Needle}${Decor1}${Decor2}${Needle}${Needle}*${Needle}${Decor2}${Needle}${Needle}${Needle}${Needle}${Decor1}${Needle}${Needle}${Decor4}${Needle}${Needle}${Decor2}${Decor4}${Decor2}${Needle}${Decor3}${Needle}${Needle}${Decor4}${Needle}${Needle}${Decor1}${Needle}${Needle}${Decor1}${Decor4}
   ${Decor4}${Decor4}${Needle}*${Decor4}${Needle}${Decor2}${Needle}*${Decor3}${Needle}${Needle}${Needle}${Needle}*${Decor1}${Needle}${Decor4}${Needle}${Needle}${Needle}${Decor4}${Needle}${Needle}${Needle}${Decor1}*${Needle}${Needle}${Decor1}${Decor3}${Needle}${Decor1}${Needle}${Needle}${Decor1}${Decor1}
  ${Needle}${Decor3}${Needle}${Needle}${Needle}${Decor4}${Needle}*${Needle}${Needle}${Decor3}${Needle}${Needle}${Decor1}${Needle}${Needle}${Decor3}${Decor1}${Decor2}${Needle}${Decor4}${Needle}*${Needle}${Decor1}${Decor2}${Needle}${Needle}${Needle}${Decor4}${Decor1}${Needle}${Decor1}${Needle}${Needle}${Needle}${Decor1}${Needle}${Decor4}
 ${Needle}${Needle}${Decor2}${Needle}${Needle}${Decor2}${Decor3}*${Needle}${Needle}${Needle}${Needle}*${Needle}${Needle}${Decor4}${Needle}${Needle}${Needle}${Needle}${Decor3}${Needle}*${Needle}${Needle}${Needle}${Decor4}${Needle}${Needle}${Needle}*${Needle}${Needle}${Needle}*${Needle}${Decor3}${Needle}${Needle}${Needle}${Needle}
                    [33m000
                    000[0m"
# Uncoment next line to periodically unfuck terminal window after resize etc
# [ "$(shuf -i 0-20 -n 1)" = "1" ] && clear
tput cnorm
}
while true ; do drawTree ; sleep 2 ; done
