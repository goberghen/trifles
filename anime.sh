#!/bin/sh
[ "$TERM" = "xterm" ] && TERM="xterm-256color"
printf '\033]2;WEABOO!\007'
clear
while :
do
	tput setab "$(shuf -i 0-255 -n 1)"
	tput setaf "$(shuf -i 0-255 -n 1)"
	echo ""
	tput ed
	tput sgr0
done
