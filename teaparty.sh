#!/bin/sh
set -o errexit -o noclobber -o nounset
# Define colors
red="[31m" ; lred="[1;31m" ; grn="[32m" ; lgrn="[1;32m" ; ylw="[33m"
lylw="[1;33m" ; cyn="[36m" ; lcyn="[1;36m" ; blu="[34m" ; lblu="[1;34m"
prp="[35m" ; lprp="[1;35m" ; rst="[0m"

Scriptname="$(basename $0)"
Sound='false'
Soundfile="$HOME/.sounds/teaparty.ogg"
Soundcmd='paplay'
Sleepcmd='sleep'

showUsage()
{
	echo "${lylw}Usage:$rst $lgrn$Scriptname$rst $lblu[OPTIONS]$rst ${lred}<NUMBER>${lprp}<SUFFIX>${rst}"
	echo "  ${lblu}Options:${rst}"
	echo "  -h|--help                   display this help"
	echo "  -s|--sound                  enable bell sound"
	echo "  -v|--visual                 show timer using 'snore' (only interactive shell)"
	echo "  ${lprp}Suffixes:${rst}"
	echo "   s                          seconds"
	echo "   m                          minutes"
	echo "   h                          hours"
	echo "${lylw}Example:${rst} $lgrn$Scriptname$rst $lblu-s$rst ${lred}7${lprp}m${rst}"
}

Opts=$(getopt -o svh --long sound,visual,help -- "$@")
eval set -- "$Opts"
while true ; do
	case "$1" in
		-s|--sound) Sound="true" ; shift ;;
		-v|--visual) command -v snore > /dev/null && [ -n "$PS1" ] && Sleepcmd="snore" ; shift ;;
		-h|--help) showUsage ; return 0 ;;
		--) shift ; break ;;
		*) echo "${lred}Nope!${rst}" ; showUsage ; return 1
	esac
done

if [ "$#" = "0" ]
then
	echo "${lred}Nope!${rst}" ; showUsage ; exit 1
fi

case "$1" in
	[0-9][smh]|[0-9][0-9][smh]|[[0-9][0-9][0-9][smh]) TIMER="$@";;
	-h|--help|"") echo $ylw"Usage:$rst $lgrn'${lylw}$Scriptname$lgrn \
	[${rst}time$lgrn][${rst}s|m|h$lgrn]$rst$lgrn'$rst" ; return 0;;
*) echo $lred"Wrong time or suffix, input numbers. \
Type$rst$lprp 'teaparty -h/--help'$rst" ; return 1;;
esac
TimerNum="$(echo ${TIMER%[[:alpha:]]})"
TimerSuff="$(echo $TIMER |\grep -o [[:alpha:]])"

# Declensions
case "$TimerSuff" in
	s) TimerSuff='секунд'
		case "$TimerNum" in
			*1) TimerSuff="${TimerSuff}у" ;;
			*[2-4]) TimerSuff="${TimerSuff}ы" ;;
		esac ;;
	m) TimerSuff='минут'
		case "$TimerNum" in
			*1) TimerSuff="${TimerSuff}у" ;;
			*[2-4]) TimerSuff="${TimerSuff}ы" ;;
		esac ;;
	h) TimerSuff='час'
		case "$TimerNum" in
			*1) TimerSuff="${TimerSuff}" ;;
			*[2-4]) TimerSuff="${TimerSuff}а" ;;
			*) TimerSuff="${TimerSuff}ов"
		esac ;;
esac

echo "It's$lcyn $(date +"%T")$rst now." && echo "Таймер установлен на $lcyn$TimerNum $TimerSuff$rst."
notify-send -a "$Scriptname" -i kteatime -u low "$Scriptname" "Таймер установлен на $TimerNum $TimerSuff"

if [ $Sound = "true" ]
then
	playSound() { "$Soundcmd" "$Soundfile" ;}
else
	playSound() { sleep 0.1 ;}
fi

"$Sleepcmd" "$TIMER" && notify-send -a "$Scriptname" -i kteatime -u critical -t 30000 "$Scriptname" \
"Время вышло! ($TimerNum $TimerSuff)" && echo "Выполнено в$lcyn $(date +"%T")$rst." && playSound &

