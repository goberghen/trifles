#!/bin/bash

question=$@
before_li=$(echo $question | awk 'BEGIN { FS=" ли "; OFS=" " }; { print $1"!" }')
after_li=$(echo $question | awk 'BEGIN { FS=" ли "; OFS=" " }; { print $2 }' |\
	sed 's/?$//')
# export before_li=$before_li
yes_no=$(shuf -i 0-1 -n 1)

if [ "$yes_no" -eq "1" ]
then
	answer="$after_li $before_li"
	answer=${answer^}
	echo $answer
elif [ "$yes_no" -eq "0" ]
then
# 	answer="$after_li [1;31mне[0m $before_li"
	answer="$after_li не $before_li"
	answer=${answer^}
	echo "$answer"
else
	echo "I am ERROR!"
fi

