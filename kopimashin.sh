#!/bin/bash
# vim: set ts=4 sw=4 tw=0 noet :
####################################################################################
## Name: kopimashin.sh
## Author: goberghen, 2016
## Description: Script that generates a loss of $10 million for the music industry
# every day. Shameless ripoff of a device created Peter "brokep" Sunde, co-founder
# of The Pirate Bay. It copies a file directly to /dev/null, thus creating a "loss"
# for the music industry.
## Depends: Nothing.
####################################################################################
Scriptname=$(basename "$0")
# Define colors
red="[31m" lred="[1;31m" grn="[32m" lgrn="[1;32m" ylw="[33m" lylw="[1;33m"
cyn="[36m" lcyn="[1;36m" blu="[34m" lblu="[1;34m" prp="[35m" lprp="[1;35m"
rst="[0m"

[ "$TERM" = "xterm" ] && TERM="xterm-256color"
[ "$TERM" = "screen" ] && TERM="screen-256color"

printf '\033]2;'Kopimashin.sh'\007'
count=0
year="$(date +%Y)"
month="$(date +%B)"
day="$(date +%d)"

file="$1"
value="$2"

usage() {
	echo "Usage: $Scriptname <file> [<value>]"
}

if [ -z "$file" ] || [ "$*" = "-h" ] || [ "$*" = "--help" ]
then
	usage
	exit 1
fi

if [ -z "$value" ]
then
	value="1.25"
fi

setup() {
	trap cleanup EXIT
}

cleanup() {
	tput cnorm
}

display() {
	count=$(expr $count + 1)
	printf "\033[0;0H"
	if [ "$TERM" = "xterm-256color" ] || [ "$TERM" = "screen-256color" ]
	then
		tput setaf $(shuf -i 0-255 -n 1)
	else
		tput setaf $(shuf -i 1-7 -n 1)
	fi
	cat <<'LOGO'
  _  __           _                     _     _             _
 | |/ /          (_)                   | |   (_)           | |
 | ' / ___  _ __  _ _ __ ___   __ _ ___| |__  _ _ __    ___| |__
 |  < / _ \| '_ \| | '_ ` _ \ / _` / __| '_ \| | '_ \  / __| '_ \
 | . \ (_) | |_) | | | | | | | (_| \__ \ | | | | | | |_\__ \ | | |
 |_|\_\___/| .__/|_|_| |_| |_|\__,_|___/_| |_|_|_| |_(_)___/_| |_|
           | |
           |_|                                          Copying...
LOGO
	tput sgr0
	printf "${lblu}Date:$rst $day $month $year\n"
	printf "${lblu}Filename:$rst $(basename "$file")\n"
	printf "${lblu}Song price:$rst \$$value\n"
	printf "${lblu}Copied tracks:$rst $count\n"
	printf "${lblu}Overall value:$rst \$$(echo "$count * $value" | bc)"
}

setup
clear
tput civis
tput clear
setterm -cursor off

while true
do
	/bin/cp "$file" /dev/null
	display
# 	periodically clear garbage characters caused by resize and accidental input
	if [ "$(shuf -i 0-50 -n 1)" = "1" ] ; then clear ; fi
done
setterm -cursor on
