#!/bin/sh
# vim: set ts=4 sw=4 tw=0 ft=sh fdm=marker noet :
################################################################################
## Name: xkcd-show.sh
## Author: goberghen, 2016-2018
## Description: Fetch and show today's xkcd comic.
## Depends: jq, curl/wget, feh, notify-send (optionally).
################################################################################
Scriptname=$(basename "$0")
# Define colors
red="[31m" lred="[1;31m" grn="[32m" lgrn="[1;32m" ylw="[33m" lylw="[1;33m"
cyn="[36m" lcyn="[1;36m" blu="[34m" lblu="[1;34m" prp="[35m" lprp="[1;35m"
rst="[0m"

# Not now
# proto="$(echo $1 | grep :// | sed -e's,^\(.*://\).*,\1,g')"
# url="$(echo ${1/$proto/})"

showHelp()
{
	echo "${lylw}Usage:$rst $lgrn$Scriptname$rst $lblu[OPTIONS]${rst}"
	echo "  ${lblu}Options:${rst}"
	echo "  -h|--help                   display this help"
	echo "  -n|--number <NUMBER>        show exact comic by its number"
	echo "  -r|--random                 show random comic"
}
Number=
set -o errexit -o noclobber -o nounset
Opts=$(getopt -o hrn:l: --long help,random,number:link: -- "$@")
eval set -- "$Opts"
while true ; do
	case "$1" in
		-n|--number) Number="$2" ; shift 2 ;;
		# Для удобства надо будет только номер из ссылки выдрать
		# и использовать его как переменную $Number, а дальше всё как обычно
		-l|--link) echo 'WIP';;
		-r|--random) Number="$(shuf -i 1-2000 -n 1)" ; shift 1 ;;
		-h|--help) showHelp ; exit 0 ;;
		--) shift ; break ;;
		*) echo "Nope!" ; showHelp ; exit 1
	esac
done

# Check if dependencies are available
if [ -n "$(command -pv notify-send)" ]
then
	Notify="notify-send"
else
	Notify="return 0 ;"
fi
if [ -z "$(command -pv feh)" ]
then
	echo "Install feh for displaying picture."
	${Notify} "Xkcd" "Install feh for displaying the picture."
	return 1
fi

# Get comic json data
if [ -n "$(command -pv curl)" ]
then
	Json="$(curl -s https://xkcd.com/${Number}/info.0.json)"
	Json="$(echo $Json | tr -d '\n')"
elif [ -n "$(command -pv wget)" ]
then
	Json="$(wget --quiet -qO- https://xkcd.com/${Number}/info.0.json)"
	Json="$(echo $Json | tr -d '\n')"
else
	echo "Install wget or curl"
	${Notify} "Xkcd" "Install wget or curl"
	return 1
fi
# Check if there is json response
if [ "$Json" = "" ]
then
	echo "Failed pulling JSON data."
	$Notify "Xkcd" "Failed pulling JSON data."
	return 1
fi

Date="$(echo ${Json} | jq --raw-output '.year, .month, .day' | paste -s -d '--\n')"
Date="$(date -d ${Date} "+%d %B %Y")"
Number="$(echo $Json | jq --raw-output '.num')"
Title="$(echo ${Json} | jq --raw-output '.title')"
Summary="$(echo ${Json} | jq --raw-output '.alt')"
Link="https://xkcd.com/${Number}"
Img="$(echo ${Json} | jq --raw-output '.img')"
imgShow() { feh --title="$Title [xkcd] - feh" "$Img" ;}
echo "${lblu}Date:${rst} ${Date}"
echo "${lblu}Current comic number:${rst} ${Number}"
echo "${lblu}Current comic title:${rst} ${Title}"
echo "${lblu}Summary:${rst} ${Summary}"
echo "${lblu}Link:${rst} ${Link}"
imgShow &
${Notify} "Xkcd" "${Date}\n#${Number}, ${Title}"
